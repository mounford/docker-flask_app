#%% Get recipes-data from the server
import psycopg2
import requests
import json as js

recipes = psycopg2.connect(user = "martino",
                              password = "LbgrR6Kf8VPjBtPV",
                              host = "35.198.122.242",
                              port = "5432",
                              database = "demo")

c = recipes.cursor()

c.execute("SELECT * FROM recipes")
dbrecipes=c.fetchall()

#%% Convert to recipe_json_py
lendbrec=len(dbrecipes)
dbrecipeshort=[[0 for i in range(2)] for j in range(lendbrec)]
x=0
while x <= lendbrec-1:
    dbrecipeshort[x][0]=dbrecipes[x][0]
    dbrecipeshort[x][1]=dbrecipes[x][6]
    x += 1
    
x=0
recipe_json_py=[0 for i in range(lendbrec)]
while x <= lendbrec-1:
    recipe_json_py[x]=dict(Key=dbrecipeshort[x][0],Value=dbrecipeshort[x][1])
    x += 1
    

#%% Json request to main.py

recipe_json = js.dumps(recipe_json_py)
r = requests.post('http://127.0.0.1:5000/postjson', json=recipe_json)
output=js.loads(r.text)
