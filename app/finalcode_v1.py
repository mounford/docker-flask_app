import sqlite3
import re
import nltk


def splitingr(recipe, language):

#%% Fetch ingredients db (local)
    conn = sqlite3.connect('ingredientslist.db')
    c = conn.cursor()
    
    if language == 'e':
        ingredients=c.execute("SELECT ingredients FROM ingredientslist").fetchall()
        ingredients = [item for sublist in ingredients for item in sublist]
        conn.close()
    elif language == 'd':
        ingredients=c.execute("SELECT zutaten FROM ingredientslist").fetchall()
        ingredients = [item for sublist in ingredients for item in sublist]
        conn.close()
    

#%% Tokenize Recipe
    lenrep=len(recipe)
    recipeingredients = [[] for _ in range(lenrep)]
    x=0     
    while x <= lenrep-1:
        recipe[x][1]= re.sub("[,.\-!?:'_]+",'', recipe[x][1])
        recipe[x][1]= recipe[x][1].casefold()
        recipeingredients[x]=nltk.word_tokenize(recipe[x][1])
        x += 1
    
#%% Make list of ingredients of each recipe
    x=0
    rec_ing = [[] for _ in range(lenrep)]
    
    while x <= lenrep-1:
        recipe_desc = recipeingredients[x]
        recipeLen = len(recipeingredients[x])
        result = []
        
        for ingredient in ingredients:
            filtered = list(filter(None, re.split("[, \-!?:_]+", ingredient.casefold())))
            
            for recep in recipe_desc:
                if filtered[0] == recep:
                    index = recipe_desc.index(recep)
                    if len(filtered) == 1: 
                        result.append(ingredient)
                        break
                    if len(filtered) == 2 and index+1 < recipeLen:
                        if filtered[1] == recipe_desc[index+1]: 
                            result.append(ingredient)
                            break
        rec_ing[x] = result
        x += 1
        
    
    
#%% Recreate list of lists with same structure as above
    widrep=len(recipe[1])
    output=[[0 for i in range(widrep)] for j in range(lenrep)]
    x=0
    while x <= lenrep-1:
        output[x][0]=recipe[x][0]
        output[x][1]=rec_ing[x]
        x += 1
    return output       #send output back!