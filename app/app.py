from flask import Flask
from flask import request
import json as js

import finalcode_v1 as f #finalcode_v1 contains the function


app = Flask(__name__)

@app.route('/', methods = ['POST'])
def postJsonHandler():
    print (request.is_json)
    input = js.loads(request.get_json())
    l=len(input)
    language=input[l-1]
    del input[-1]
    l=len(input)
    recipe=[[0 for i in range(2)] for j in range(l)]
    x=0
    while x <= l-1:
        recipe[x][0]=input[x]['Key']
        recipe[x][1]=input[x]['Value']
        x += 1
    recipesplitted = f.splitingr(recipe,language)
    return js.dumps(recipesplitted)
    
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)