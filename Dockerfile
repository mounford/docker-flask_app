FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y build-essential python3-minimal python3-pip

COPY ./app /app
WORKDIR /app

RUN cd /app && pip3 install -r requirements.txt

RUN chmod +x /app/install_punkt.py
RUN python3.6 /app/install_punkt.py

ENTRYPOINT ["python3.6"]
CMD ["app.py"]